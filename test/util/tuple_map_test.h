#pragma once

#include <gtest/gtest.h>
#include <util/tuple_map.h>


TEST(Tuplemap, end_to_end)
{
    TupleMap<int> map;
    map.emplace(5, 1, 2, 3);
    map.emplace(7, "abc");
    map.emplace(8, 3, 2, 1);
    map.emplace(9, "def");
    ASSERT_EQ(map(1,2,3), 5);
    ASSERT_EQ(map("def"), 9);
    ASSERT_EQ(map(3,2,1), 8);
    ASSERT_EQ(map("abc"), 7);

    ASSERT_EQ(map.contains("abc"), true);
    ASSERT_EQ(map.contains(3,3,3), false);
    ASSERT_EQ(map.contains(3,2,1), true);
    ASSERT_EQ(map.contains("xyz"), false);
}
