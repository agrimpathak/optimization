#include <gtest/gtest.h>

#include <linear/lp_test.h>
#include <linear/mip_test.h>
#include <util/tuple_map_test.h>


int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
