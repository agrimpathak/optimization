#pragma once

#include <gtest/gtest.h>
#include <linear/constraint.h>
#include <linear/lp.h>
#include <linear/var.h>
#include <util/constants.h>
#include <util/tuple_map.h>


TEST(LinearProgram, solve)
{
    LinearProgram lp;
    lp.set_sense(optimization_sense::maximize);
    lp.add_var(8.0,  0.0, INF, "x");
    lp.add_var(12.0, 0.0, INF, "y");

    const TupleMap<Var>& vars = lp.vars();

    Constraint c1 = lp.add_constr(-INF, 140.0);
    c1.add_term(10.0, vars("x"));
    c1.add_term(20.0, vars("y"));

    Constraint c2 = lp.add_constr(-INF, 72.0);
    c2.add_term(6.0, vars("x"));
    c2.add_term(8.0, vars("y"));

    lp.solve();

    const double tol = 1e-5;
    ASSERT_NEAR(lp.solution("x"), 8.0, tol);
    ASSERT_NEAR(lp.solution("y"), 3.0, tol);
    ASSERT_NEAR(vars("x").sol(), 8.0, tol);
    ASSERT_NEAR(vars("y").sol(), 3.0, tol);
}
