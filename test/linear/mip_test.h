#pragma once

#include <gtest/gtest.h>
#include <linear/constraint.h>
#include <linear/mip.h>
#include <linear/var.h>
#include <util/constants.h>
#include <util/tuple_map.h>


TEST(MixedIntegerProgram, solve_lp)
{
    MixedIntegerProgram mip;
    mip.set_sense(optimization_sense::maximize);
    mip.add_var(1.0,  0.0, 3.5, false, "x");
    mip.add_var(10.0, 0.0, INF, false, "y");

    const TupleMap<Var>& vars = mip.vars();

    Constraint c1 = mip.add_constr(-INF, 17.5);
    c1.add_term(1.0, vars("x"));
    c1.add_term(7.0, vars("y"));

    mip.solve();

    const double tol = 1e-5;
    ASSERT_NEAR(mip.solution("x"), 0.0, tol);
    ASSERT_NEAR(mip.solution("y"), 2.5, tol);
    ASSERT_NEAR(vars("x").sol(), 0.0, tol);
    ASSERT_NEAR(vars("y").sol(), 2.5, tol);
}


TEST(MixedIntegerProgram, solve_mip)
{
    MixedIntegerProgram mip;
    mip.set_sense(optimization_sense::maximize);
    mip.add_var(1.0,  0.0, 3.5, true, "x");
    mip.add_var(10.0, 0.0, INF, true, "y");

    const TupleMap<Var>& vars = mip.vars();

    Constraint c1 = mip.add_constr(-INF, 17.5);
    c1.add_term(1.0, vars("x"));
    c1.add_term(7.0, vars("y"));

    mip.solve();

    const double tol = 1e-5;
    ASSERT_NEAR(mip.solution("x"), 3.0, tol);
    ASSERT_NEAR(mip.solution("y"), 2.0, tol);
    ASSERT_NEAR(vars("x").sol(), 3.0, tol);
    ASSERT_NEAR(vars("y").sol(), 2.0, tol);
}
