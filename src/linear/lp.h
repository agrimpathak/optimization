#pragma once

#include <coin/ClpSimplex.hpp>
#include "linear_model.h"


class LinearProgram : public LinearModel
{
public:

    LinearProgram()
    {
        _mdl.setLogLevel(0);
    }


    void set_sense(const optimization_sense::optimization_sense_t sense)
    {
        _mdl.setOptimizationDirection(sense);
    }


    bool feasible() const
    {
        return _mdl.primalFeasible();
    }


    void solve()
    {
        build_constraint_matrix();
        _mdl.loadProblem(
            *_A            ,
            _col_lb.data() ,
            _col_ub.data() ,
            _obj.data()    ,
            _row_lb.data() ,
            _row_ub.data()
        );
        _mdl.primal();
        _sol = _mdl.primalColumnSolution();
    }


    double obj_val() const
    {
        return _mdl.objectiveValue();
    }


private:
        ClpSimplex _mdl;

};
