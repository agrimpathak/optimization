#pragma once

#include <memory>
#include <string>
#include <vector>
#include <coin/CoinPackedMatrix.hpp>
#include <math/coordinate_matrix.h>
#include <util/macro.h>
#include <util/tuple_map.h>
#include "constraint.h"
#include "linear_model_types.h"
#include "var.h"


namespace optimization_sense
{
    typedef double optimization_sense_t;
    static constexpr optimization_sense_t minimize = +1.0;
    static constexpr optimization_sense_t maximize = -1.0;
}


class LinearModel
{
private:

    inline const static std::string DEFAULT_NAME_PREFIX = "__COLUMN__";

public:

    size_t n_vars() const
    {
        __ASSERT__(_obj.size() == _col_lb.size());
        __ASSERT__(_obj.size() == _col_ub.size());
        return _vars.size();
    }


    size_t n_constr() const
    {
        __ASSERT__(_row_lb.size() == _row_ub.size());
        return _row_lb.size();
    }


    template<typename ...Args>
    const Var& add_var(const elem_t obj ,
                       const elem_t lb  ,
                       const elem_t ub  ,
                       Args&&... args   )
    {
        __ASSERT__(_obj.size() == _col_lb.size());
        __ASSERT__(_obj.size() == _col_ub.size());
        __ASSERT__(!_vars.contains(args...));
        _obj.push_back(obj);
        _col_lb.push_back(lb);
        _col_ub.push_back(ub);
        return _vars.emplace(
                Var(static_cast<index_t>(n_vars()), &_sol), args...)->second;
    }


    template<typename ...Args>
    const Var& add_var(const elem_t obj ,
                       const elem_t lb  ,
                       const elem_t ub  )
    {
        return add_var(
                obj, lb, ub, DEFAULT_NAME_PREFIX + std::to_string(n_vars()));
    }


    Constraint add_constr(const elem_t row_lb, const elem_t row_ub)
    {
        const index_t row = static_cast<index_t>(n_constr());
        _row_lb.push_back(row_lb);
        _row_ub.push_back(row_ub);
        return Constraint(_coord, row);
    }


    const TupleMap<Var>& vars() const
    {
        return _vars;
    }


    const double* solution() const
    {
        return _sol;
    }


    template<typename ...Args>
    double solution(Args&&... args) const
    {
        return solution()[_vars(args...).col()];
    }


protected:

    void build_constraint_matrix()
    {
        _A.reset(
            new CoinPackedMatrix(
                false                               ,
                _coord.rows()                       ,
                _coord.cols()                       ,
                _coord.elems()                      ,
                static_cast<index_t>(_coord.size())
            )
        );
    }


protected:
        const double* _sol;

        std::vector<elem_t> _obj;

        std::vector<elem_t> _col_lb;

        std::vector<elem_t> _col_ub;

        std::vector<elem_t> _row_lb;

        std::vector<elem_t> _row_ub;

        TupleMap<Var> _vars;

        CoordinateMatrix<index_t> _coord;

        std::unique_ptr<CoinPackedMatrix> _A;
};
