#pragma once

#include <memory>
#include <vector>
#include <coin/CbcModel.hpp>
#include <coin/OsiClpSolverInterface.hpp>
#include "linear_model.h"
#include "linear_model_types.h"


class MixedIntegerProgram : public LinearModel
{
public:

    void set_sense(const optimization_sense::optimization_sense_t sense)
    {
        _solver.setObjSense(sense);
    }


    bool feasible() const
    {
        return !_mdl->isProvenInfeasible();
    }


    template<typename ...Args>
    const Var& add_var(const elem_t obj      ,
                       const elem_t lb       ,
                       const elem_t ub       ,
                       const bool is_integer ,
                       Args&&... args        )
    {
        if (is_integer)
        {
            _integer_idx.push_back(static_cast<index_t>(n_vars()));
        }
        return LinearModel::add_var(obj, lb, ub, args...);
    }


    template<typename ...Args>
    const Var& add_var(const elem_t obj      ,
                       const elem_t lb       ,
                       const elem_t ub       ,
                       const bool is_integer )
    {
        if (is_integer)
        {
            _integer_idx.push_back(static_cast<index_t>(n_vars()));
        }
        return LinearModel::add_var(obj, lb, ub);
    }


    void solve()
    {
        build_constraint_matrix();
        _solver.loadProblem(
            *_A            ,
            _col_lb.data() ,
            _col_ub.data() ,
            _obj.data()    ,
            _row_lb.data() ,
            _row_ub.data()
        );
        _solver.setInteger(
                _integer_idx.data(), static_cast<int>(_integer_idx.size()));
        _mdl.reset(new CbcModel(_solver));
        _mdl->setLogLevel(0);
        _mdl->branchAndBound();
        _sol = _mdl->bestSolution();
    }


    double obj_val() const
    {
        return _mdl->getObjValue();
    }


private:
        OsiClpSolverInterface _solver;

        std::unique_ptr<CbcModel> _mdl;

        std::vector<index_t> _integer_idx;
};
