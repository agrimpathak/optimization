#pragma once

#include <math/coordinate_matrix.h>
#include "linear_model_types.h"
#include "var.h"


class Constraint
{
friend class LinearModel;

public:

    Constraint(const Constraint&)            = delete;
    Constraint(Constraint&&)                 = delete;
    Constraint& operator=(const Constraint&) = delete;
    Constraint& operator=(Constraint&&)      = delete;


    void add_term(const elem_t coef, const Var& var)
    {
        _coord.add_elem(_row, var.col(), coef);
    }


private:

    Constraint(CoordinateMatrix<index_t, elem_t>& coord ,
               const index_t row                        ) :
        _coord(coord) ,
        _row(row)
    { }

private:

    CoordinateMatrix<index_t, elem_t>& _coord;

    const index_t _row;
};
