#pragma once

#include "linear_model_types.h"


class Var
{
friend class LinearModel;

public:

    double sol() const
    {
        return *(*_sol + _col);
    }


    index_t col() const
    {
        return _col;
    }

private:

    Var(const index_t col              ,
        const double* const* const sol ) :
        _col(col) ,
        _sol(sol)
    { }


private:

    const index_t _col;

    const double* const* const _sol;
};
