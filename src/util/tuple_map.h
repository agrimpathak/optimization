#pragma once

#include <sstream>
#include <string>
#include <tuple>
#include <unordered_map>
#include "serialize_tuple.h"


template<typename V>
class TupleMap
{
public:

    typedef typename std::unordered_map<std::string, V>::const_iterator
        const_iterator;

public:

    template<typename ...Args>
    const_iterator emplace(V&& value, Args&&... key_args)
    {
        static_assert(sizeof...(key_args) > 0);
        return _container.emplace(gen_key(key_args...), value).first;
    }


    template<typename ...Args>
    const_iterator find(Args&&... args) const
    {
        return _container.find(gen_key(args...));
    }


    template<typename ...Args>
    const V& operator()(Args&&... args) const
    {
        return find(args...)->second;
    }


    template<typename ...Args>
    bool contains(Args&&... args) const
    {
        return find(args...) != cend();
    }


    const_iterator cbegin() const
    {
        return _container.cbegin();
    }


    const_iterator cend() const
    {
        return _container.cend();
    }


    const_iterator begin() const
    {
        return cbegin();
    }


    const_iterator end() const
    {
        return cend();
    }


    size_t size() const
    {
        return _container.size();
    }

private:

    template<typename K>
    std::string gen_key(const K& k) const
    {
        std::stringstream ss;
        ss << k;
        return ss.str();
    }


    template<typename ...Args>
    std::string gen_key(Args&&... args) const
    {
        return serialize_tuple(std::make_tuple(args...));
    }

private:

    std::unordered_map<std::string, V> _container;
};
