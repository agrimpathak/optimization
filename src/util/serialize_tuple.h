#pragma once

#include <sstream>
#include <tuple>


template<size_t N>
struct TupleSerializer
{
    template<typename ...Args>
    static void serialize(const std::tuple<Args...>& t, std::stringstream& ss)
    {
        ss << std::get<N-1>(t) << ",";
        TupleSerializer<N-1>::serialize(t, ss);
    }
};


template<>
struct TupleSerializer<0>
{
    template<typename ...Args>
    static void serialize(const std::tuple<Args...>& t, std::stringstream& ss)
    {
        (void) (t);
        ss << ")";
    }
};


template<typename ...Args>
std::string serialize_tuple(const std::tuple<Args...>& t)
{
    static_assert(std::tuple_size<std::tuple<Args...>>::value > 0);
    std::stringstream ss;
    ss << "(";
    TupleSerializer<
        std::tuple_size<std::tuple<Args...>>::value>::serialize(t, ss);
    return ss.str();
}
