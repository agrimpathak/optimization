#pragma once

#ifdef DEBUG
#include <cassert>
#include <iostream>
#endif

#ifdef DEBUG
#define __DEBUG__(x) x
#else
#define __DEBUG__(x)
#endif


#ifdef DEBUG
#define __PRINT__(x) std::cout << x;
#else
#define __PRINT__(x)
#endif


#ifdef DEBUG
#define __PRINTL__(x) std::cout << x << "\n";
#else
#define __PRINTL__(x)
#endif


#ifdef DEBUG
#define __ASSERT__(x) assert(x);
#else
#define __ASSERT__(x)
#endif
