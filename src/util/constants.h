#pragma once

#include <limits>


constexpr double INF = std::numeric_limits<double>::max();
