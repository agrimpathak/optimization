#pragma once

#include <vector>
#include <util/macro.h>


template<typename INDEX_TYPE = size_t ,
         typename ELEM_TYPE = double  >
class CoordinateMatrix
{
public:

    void add_elem(const INDEX_TYPE row ,
                  const INDEX_TYPE col ,
                  const ELEM_TYPE elem )
    {
        _rows.push_back(row);
        _cols.push_back(col);
        _elems.push_back(elem);
    }


    const INDEX_TYPE* rows() const
    {
        return _rows.data();
    }


    const INDEX_TYPE* cols() const
    {
        return _cols.data();
    }


    const ELEM_TYPE* elems() const
    {
        return _elems.data();
    }


    size_t size() const
    {
        return _rows.size();
    }

private:

    std::vector<INDEX_TYPE> _rows;

    std::vector<INDEX_TYPE> _cols;

    std::vector<ELEM_TYPE> _elems;
};
